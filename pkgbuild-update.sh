#!/bin/sh -e

package="$1"
version="$2"

git clone https://gitlab.com/archlinux-php/packages/$package.git

current=`sed -n 's/pkgver=\(.*\)/\1/p' < $package/PKGBUILD`
release=`curl --silent https://www.php.net/releases/?json\&version=$version | jq -r '.source[2].filename' | cut -c5-10`

if [ "$release" != "$current" ]; then
        hash=`curl --silent https://www.php.net/releases/?json\&version=$version | jq -r '.source[2].sha256'`
        sed -i "/pkgver=/c\pkgver=$release" $package/PKGBUILD
        sed -i "/pkgrel=/c\pkgrel=1" $package/PKGBUILD
        sed -i "/sha256sums=/c\sha256sums=('$hash'" $package/PKGBUILD   
        git --git-dir $package/.git --work-tree=$package add PKGBUILD
        git --git-dir $package/.git --work-tree=$package commit -m "$package $release"
        git --git-dir $package/.git --work-tree=$package remote set-url --push origin https://$GIT_ACCESS_USER:$GIT_ACCESS_TOKEN@gitlab.com/archlinux-php/packages/$package.git
        git --git-dir $package/.git --work-tree=$package push -u origin master
fi
